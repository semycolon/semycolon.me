const express = require('express');
const path = require('path');
const fs = require('fs');
const app = express();
const port = 3003;
const bodyParse = require('body-parser');
app.use(bodyParse.urlencoded({extended: false}));
app.use(bodyParse.json());

app.use(express.static(path.resolve('../website/')));

app.get('/', (req, res) => res.sendFile(path.resolve('../website/index.html')));
app.post('/contact', (req, res) => {
    let body = req.body;
    console.log("got message", body);
    let file_path = './messages.json';
    try {
        fs.statSync(file_path)
    } catch (err) {
        fs.writeFileSync(file_path, JSON.stringify({messages: []}), {encoding: 'utf-8'})
    }

    let obj = JSON.parse(fs.readFileSync(file_path, {encoding: 'utf-8'}));
    obj.messages.push({id: obj.messages.length + 1, date: new Date(), ...body});

    fs.writeFileSync(file_path, JSON.stringify(obj), {encoding: 'utf-8'});

    res.send("got your message :)");
});
app.listen(port, () => console.log(`Example app listening on http://localhost:${port}`));
